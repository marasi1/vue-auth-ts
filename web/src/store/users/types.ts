export interface APIResponse {
	name: string;
	email: string;
	token: string;
}

export interface UserType {
	name: string;
	email: string;
}

export interface UserForm {
	email: string;
	password: string;
}

export enum MutationTypes {
	SET_USER = "SET_USER",
	REMOVE_USER = "REMOVE_USER",
}
