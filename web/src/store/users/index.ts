import {
	Module,
	Action,
	MutationAction,
	Mutation,
	VuexModule,
} from "vuex-module-decorators";
import axios, { AxiosResponse } from "axios";

import { UserType, APIResponse, MutationTypes, UserForm } from "./types";

@Module({
	namespaced: true,
})
class User extends VuexModule {
	user: UserType | null = null;

	@Mutation
	[MutationTypes.SET_USER](payload: UserType): void {
    console.log(payload, 'payload store')
		this.user = payload;
	}

	@Mutation
	[MutationTypes.REMOVE_USER](): void {
		this.user = null;
	}

	@MutationAction({ mutate: ["user"] })
	async postRegister(form: UserType): Promise<APIResponse | any> {
		const response: AxiosResponse<APIResponse> = await axios({
			method: "post",
			url: "http://localhost:3000/signup",
			data: form,
		});

		const { data } = response;
		const { name, email, token } = data;

		localStorage.setItem("_auth", token);
		axios.defaults.headers.common["Authorization"] = token;

		return {
			user: {
				name,
				email,
			},
		};
	}

	@MutationAction({ mutate: ["user"] })
	async postSignIn(form: UserForm): Promise<APIResponse | any> {
		const response: AxiosResponse<APIResponse> = await axios({
			method: "post",
			url: "http://localhost:3000/signin",
			data: form,
		});

		const { data } = response;
		const { name, email, token } = data;
		console.log(response.data);
		localStorage.setItem("_auth", token);
		localStorage.setItem("_user", JSON.stringify({ name, email }));
		axios.defaults.headers.common["Authorization"] = token;

		return {
			user: {
				name,
				email,
			},
		};
	}

	@Action({})
	async postSignOut(): Promise<{ message: string } | any> {
		const response: AxiosResponse<{ message: string }> = await axios({
			method: "post",
			url: "http://localhost:3000/signout",
		});

		localStorage.removeItem("_auth");
		localStorage.removeItem("_user");
		delete axios.defaults.headers.common["Authorization"];

		this.context.commit(MutationTypes.REMOVE_USER);
	}

	get isAuthenticated(): boolean {
		return Boolean(this.user?.name);
	}
}

export default User;
