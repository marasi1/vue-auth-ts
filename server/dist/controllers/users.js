"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../models/User");
const password_1 = __importDefault(require("../helpers/password"));
const token_1 = __importDefault(require("../helpers/token"));
exports.postRegister = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { fullName, email, password } = req.body;
        const newUserInstance = new User_1.User({
            fullName,
            email,
            password,
        });
        const newUser = yield newUserInstance.save();
        const token = token_1.default.generateToken({
            id: newUser._id,
            name: newUser.fullName,
        });
        return res.status(201).json({
            name: newUser.fullName,
            email: newUser.email,
            token,
        });
    }
    catch (error) {
        next(error);
    }
});
exports.postSignIn = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email, password } = req.body;
        const foundUser = yield User_1.User.findOne({ email });
        if (foundUser == null) {
            return res.status(404).json({
                error: {
                    code: 404,
                    message: "User is not found",
                },
            });
        }
        const isMatch = yield password_1.default(password, foundUser.password);
        if (!isMatch) {
            return res.status(400).json({
                error: {
                    code: 400,
                    message: "Invalid credentials",
                },
            });
        }
        const token = yield token_1.default.generateToken({
            id: foundUser._id,
            name: foundUser.fullName,
        });
        return res.json({
            name: foundUser.fullName,
            email: foundUser.email,
            token,
        });
    }
    catch (error) {
        next(error);
    }
});
exports.postSignOut = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = res.locals.user;
        const foundUser = yield User_1.User.findById(id);
        console.log(foundUser);
        if (foundUser == null) {
            return res.status(404).json({
                error: {
                    code: 404,
                    message: "User is not found",
                },
            });
        }
        return res.json({
            message: "Signed out",
        });
    }
    catch (error) {
        next(error);
    }
});
exports.getAuth = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return res.json({
            message: "You are authenticated",
        });
    }
    catch (error) {
        next(error);
    }
});
//# sourceMappingURL=users.js.map