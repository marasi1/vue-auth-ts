"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
const lusca_1 = __importDefault(require("lusca"));
const mongoose_1 = __importDefault(require("mongoose"));
const morgan_1 = __importDefault(require("morgan"));
const auth_1 = require("./middlewares/auth");
const users_1 = require("./controllers/users");
dotenv_1.default.config();
const app = express_1.default();
app.set("port", process.env.PORT || 3000);
app.set("host", "0.0.0.0");
const mongoURL = process.env.MONGODB_URI || "mongodb://localhost:27017/vue-auth";
mongoose_1.default
    .connect(mongoURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
})
    .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
})
    .catch((err) => {
    console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
    process.exit();
});
app.use(compression_1.default());
app.use(cors_1.default());
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(lusca_1.default.xframe("SAMEORIGIN"));
app.use(lusca_1.default.xssProtection(true));
app.use(morgan_1.default("tiny"));
app.get("/", (req, res) => {
    res.json({
        data: {
            message: "Server is up and running",
        },
    });
});
app.post("/signup", users_1.postRegister);
app.post("/signin", users_1.postSignIn);
app.get("/auth", auth_1.checkAuthentication, users_1.getAuth);
app.post("/signout", auth_1.checkAuthentication, users_1.postSignOut);
exports.default = app;
//# sourceMappingURL=app.js.map