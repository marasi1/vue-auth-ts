import { Request, Response, NextFunction } from "express";
import app from "./app";

app.use((error: Error, _: Request, res: Response, __: NextFunction) => {
	const { message, stack, name } = error;
	return res.status(500).json({
		error: {
			code: 500,
			name,
			message,
			stack,
		},
	});
});

/**
 * Start Express server.
 */
const server = app.listen(app.get("port"), () => {
	console.log(
		"  App is running at http://localhost:%d in %s mode",
		app.get("port"),
		app.get("env")
	);
	console.log("  Press CTRL-C to stop\n");
});

export default server;
