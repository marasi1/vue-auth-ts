import { Request, Response, NextFunction } from "express";

import { User } from "../models/User";
import comparePassword from "../helpers/password";
import tokenModule from "../helpers/token";

export const postRegister = async (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	try {
		const { fullName, email, password } = req.body;

		const newUserInstance = new User({
			fullName,
			email,
			password,
		});
		const newUser = await newUserInstance.save();

		const token = tokenModule.generateToken({
			id: newUser._id,
			name: newUser.fullName,
		});

		return res.status(201).json({
			name: newUser.fullName,
			email: newUser.email,
			token,
		});
	} catch (error) {
		next(error);
	}
};

export const postSignIn = async (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	try {
		const { email, password } = req.body;

		const foundUser = await User.findOne({ email });

		if (foundUser == null) {
			return res.status(404).json({
				error: {
					code: 404,
					message: "User is not found",
				},
			});
		}

		const isMatch = await comparePassword(password, foundUser.password);

		if (!isMatch) {
			return res.status(400).json({
				error: {
					code: 400,
					message: "Invalid credentials",
				},
			});
		}

		const token = await tokenModule.generateToken({
			id: foundUser._id,
			name: foundUser.fullName,
		});

		return res.json({
			name: foundUser.fullName,
			email: foundUser.email,
			token,
		});
	} catch (error) {
		next(error);
	}
};

export const postSignOut = async (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	try {
		const { id } = res.locals.user;

		const foundUser = await User.findById(id);
		console.log(foundUser);
		if (foundUser == null) {
			return res.status(404).json({
				error: {
					code: 404,
					message: "User is not found",
				},
			});
		}

		return res.json({
			message: "Signed out",
		});
	} catch (error) {
		next(error);
	}
};

export const getAuth = async (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	try {
		return res.json({
			message: "You are authenticated",
		});
	} catch (error) {
		next(error);
	}
};
