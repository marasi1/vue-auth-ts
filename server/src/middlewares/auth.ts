import { Request, Response, NextFunction } from "express";
import tokenModule from "../helpers/token";

export const checkAuthentication = async (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	try {
		const { authorization } = req.headers;

		if (authorization) {
			const foundMatch = await tokenModule.verifyToken(authorization);
			if (foundMatch == null) {
				return res.status(401).json({
					error: {
						code: 400,
						message: "Bad Input",
					},
				});
			}
			res.locals.user = foundMatch;

			return next();
		} else {
			res.status(401).json({
				error: {
					code: 401,
					message: "Unauthenticated",
				},
			});
		}
	} catch (error) {
		next(error);
	}
};
