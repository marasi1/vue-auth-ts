import mongoose from "mongoose";
import bcrypt from "bcrypt";

export type UserDocument = mongoose.Document & {
	fullName: string;
	email: string;
	password: string;
};

const userSchema = new mongoose.Schema(
	{
		fullName: String,
		email: { type: String, unique: true },
		password: String,
	},
	{ timestamps: true }
);

userSchema.pre("save", function save(next) {
	const user = this as UserDocument;

	if (!user.isModified("password")) {
		return next();
	}
	bcrypt.genSalt(10, (err, salt) => {
		if (err) {
			return next(err);
		}
		bcrypt.hash(user.password, salt, (err: mongoose.Error, hash: string) => {
			if (err) {
				return next(err);
			}
			user.password = hash;
			next();
		});
	});
});

export const User = mongoose.model<UserDocument>("User", userSchema);
