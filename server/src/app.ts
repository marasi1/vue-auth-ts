import express from "express";
import compression from "compression";
import cors from "cors";
import dotenv from "dotenv";
import lusca from "lusca";
import mongoose from "mongoose";
import morgan from "morgan";

import { checkAuthentication } from "./middlewares/auth";
import {
	postRegister,
	postSignIn,
	postSignOut,
	getAuth,
} from "./controllers/users";

dotenv.config();

const app = express();

app.set("port", process.env.PORT || 3000);
app.set("host", "0.0.0.0");

const mongoURL: string =
	process.env.MONGODB_URI || "mongodb://localhost:27017/vue-auth";

mongoose
	.connect(mongoURL, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		/** ready to use. The `mongoose.connect()` promise resolves to undefined. */
	})
	.catch((err) => {
		console.log(
			"MongoDB connection error. Please make sure MongoDB is running. " + err
		);
		process.exit();
	});

app.use(compression());
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));

app.use(morgan("tiny"));

app.get("/", (req, res) => {
	res.json({
		data: {
			message: "Server is up and running",
		},
	});
});

app.post("/signup", postRegister);
app.post("/signin", postSignIn);
app.get("/auth", checkAuthentication, getAuth);
app.post("/signout", checkAuthentication, postSignOut);

export default app;
