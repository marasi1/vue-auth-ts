import bcrypt from "bcrypt";

export default async function comparePassword(
	candidatePassword: string,
	hash: string
): Promise<boolean | object> {
	try {
		const isMatch = await bcrypt.compare(candidatePassword, hash);

		return isMatch;
	} catch (error) {
		return error;
	}
}
