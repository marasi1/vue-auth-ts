import jwt from "jsonwebtoken";

interface TokenPayload {
	id: string;
	name: string;
}

export default {
	generateToken: async function generateToken(
		arg: TokenPayload
	): Promise<string | object> {
		try {
			const token = jwt.sign(arg, process.env.JWT_SECRET);

			return token;
		} catch (error) {
			return error;
		}
	},

	verifyToken: async function verifyToken(
		token: string
	): Promise<string | object> {
		try {
			const isMatch = jwt.verify(token, process.env.JWT_SECRET);

			return isMatch;
		} catch (error) {
			return error;
		}
	},
};
